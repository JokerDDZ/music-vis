# Music Vis

## Technology 

### Unity,version: 2020.3.17f1

 [Download](https://unity3d.com/get-unity/download/archive)

### LaspLibrary:

[Github](https://github.com/keijiro/Lasp)

### Graphy - Ultimate FPS Counter

[AssetStore](https://assetstore.unity.com/packages/tools/gui/graphy-ultimate-fps-counter-stats-monitor-debugger-105778)

### Render Pipeline: URP

### docs: music-vis\Music Vis\docs
### build: music-vis\Music Vis\dist
