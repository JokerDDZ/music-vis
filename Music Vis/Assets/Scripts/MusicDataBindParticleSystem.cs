using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using System.Linq;

public class MusicDataBindParticleSystem : MonoBehaviour
{
    [SerializeField] private GUIController _guiController;
    [SerializeField] private PlayableDirector _playableDirector;
    [SerializeField] private List<ParticlesCompute2D> _particlesCompute2D;
    private const int _numberOfSamples = 300;
    private float[] _avaForParticles;
    private float[] _freqBuffer;
    private float[] _bandBuffer;
    private float[] _bufferDecrease;
    private float _speedTrans = 0f;
    private bool _trans = false;
    private Color _colorL;
    private Color _colorM;
    private Color _colorH;

    #region Settings
    private mySettings _mySettings;
    private Settingslst _thissettings;
    private const string _nameOfClass = "ComputeShader";
    #endregion

    private void Awake()
    {
        _freqBuffer = new float[_numberOfSamples];
        _bandBuffer = new float[_numberOfSamples];
        _bufferDecrease = new float[_numberOfSamples];
        _avaForParticles = new float[_particlesCompute2D.Count];

        _playableDirector.RebuildGraph();
    }

    private void Start()
    {
        GUIElementController speed = _guiController.GetElementController("Speed");
        speed.SetMinMaxFloat(0f, 10f);

        _mySettings = new mySettings();
        if (!_mySettings.Settingslst.Where(s => s.className == _nameOfClass).Any())
        {
            _mySettings.Settingslst.Add(new Settingslst { className = _nameOfClass });
            _mySettings.Save();
            _mySettings.Read();
        }

        _thissettings = _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault();

        GUIElementController trans = _guiController.GetElementController("Transition");
        GUIElementController colorL = _guiController.GetElementController("ColorL");
        GUIElementController colorM = _guiController.GetElementController("ColorM");
        GUIElementController colorH = _guiController.GetElementController("ColorH");
        trans.SetToggle(_thissettings.clockwise);
        speed.SetSlider(_thissettings.rotation_speed);
        colorL.SetColorPicker(_thissettings.ColorL);
        colorM.SetColorPicker(_thissettings.ColorMid);
        colorH.SetColorPicker(_thissettings.ColorS);
    }

    private void Update()
    {
        Savesettings();

        _colorL = _guiController.ReturnValueFrom<Color>("ColorL");
        _colorM = _guiController.ReturnValueFrom<Color>("ColorM");
        _colorH = _guiController.ReturnValueFrom<Color>("ColorH");
        _particlesCompute2D[0].SetColor = _colorL;
        _particlesCompute2D[1].SetColor = _colorM;
        _particlesCompute2D[2].SetColor = _colorH;
        _speedTrans = _guiController.ReturnValueFrom<float>("Speed");
        _trans = _guiController.ReturnValueFrom<bool>("Transition");

        if (!_trans)
        {
            _playableDirector.Stop();
        }
        else
        {
            _playableDirector.Play();
            _playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(_speedTrans);
        }
    }

    private void Savesettings()
    {
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().clockwise = _trans;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().rotation_speed = _speedTrans;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().ColorL = _colorL;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().ColorMid = _colorM;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().ColorS = _colorH;
        _mySettings.Save();

    }
}
