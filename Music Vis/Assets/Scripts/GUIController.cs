using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Tayx.Graphy;

public class GUIController : MonoBehaviour
{
    [SerializeField] private GameObject _guiParent;
    [SerializeField] private GameObject _guiPanel;
    [SerializeField] private Transform _content;
    [SerializeField] private GameObject _graph;
    [SerializeField] private List<PresetsByIndex> _menuPresets = new List<PresetsByIndex>();
    private List<GUIElementController> _elementControllers = new List<GUIElementController>();


    private void Awake()
    {
        foreach (PresetsByIndex m in _menuPresets)
        {
            InitMenuPreset(m);
        }

        StartCoroutine(DelayPanelOff());
    }

    private IEnumerator DelayPanelOff()
    {
        for (int i = 0; i < 2; i++)
        {
            yield return 0;
        }

        _guiPanel.SetActive(false);
    }

    void Start()
    {
        if (!_graph) _graph = FindObjectOfType<GraphyManager>(true).gameObject;

        if (_graph)
        {
            _graph.SetActive(false);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            HideGUI();
        }
    }

    public dynamic ReturnValueFrom<T>(string id)
    {
        GUIElementController element = _elementControllers.Find(x => x.GetID == id);

        if (element == null)
        {
            Debug.LogError("Cant find element controller with id: " + id);
            return null;
        }
        else
        {
            return element.getValue<T>();
        }
    }

    public GUIElementController GetElementController(string id)
    {
        GUIElementController element = _elementControllers.Find(x => x.GetID == id);

        if (element == null)
        {
            Debug.LogError("Cant find element controller with id: " + id);
            return null;
        }
        else
        {
            return element;
        }
    }

    private void HideGUI()
    {
        bool forPanels = !_guiPanel.activeSelf;
        _guiPanel.SetActive(forPanels);

        if (_graph)
        {
            _graph.SetActive(forPanels);
        }
    }

    private void InitMenuPreset(PresetsByIndex m)
    {
        GameObject goHandler = Instantiate(m.Preset.GetPrefab, _content);
        GUIElementController contr = goHandler.GetComponent<GUIElementController>();
        contr.SetHeader = m.Header;
        contr.SetID = m.ID;
        _elementControllers.Add(contr);
    }

    [Serializable]
    public struct PresetsByIndex
    {
        public MenuPresetSO Preset;
        public string ID;
        public string Header;
    }
}
