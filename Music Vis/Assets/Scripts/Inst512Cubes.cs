﻿using UnityEngine;
using Lasp;
using Unity.Collections;
using System.Linq;

public class Inst512Cubes : MonoBehaviour
{
    private const int _numberOfSamples = 300;
    private float[] _freqBuffer;
    private float[] _bandBuffer;
    private float[] _bufferDecrease;

    [SerializeField] private GUIController _guiController;
    [SerializeField] private Transform _cameraParent;
    [SerializeField] private Light _light;
    public GameObject SampleCubePrefab;
    private GameObject[] _sampleCube;
    private GameObject[] _middleSampleCubes;
    [SerializeField] private Gradient _gradientColor;
    [SerializeField] private SpectrumAnalyzer _spectrumAnalyzer;
    [SerializeField] private float _scaleMulti = 100f;
    [SerializeField] private float _speedOfLerp = 5f;
    private Color _colorF;
    private Color _colorL;
    private Color _colorMid;
    private Color _lightColor;
    private const int _quantityOfMiddleCubes = 4;
    private float _maxHDRInten = 8f;
    private float _rotatingSpeed = 1f;
    private bool _rotateCamera = false;
    private bool _clockwise = false;
    private float _lightIntensityMulit = 1000f;

    #region Settings
    private mySettings _mySettings;
    private Settingslst _thissettings;
    private const string _nameOfClass = "Cubes";
    #endregion

    private void Awake()
    {
        _freqBuffer = new float[_numberOfSamples];
        _bandBuffer = new float[_numberOfSamples];
        _bufferDecrease = new float[_numberOfSamples];
    }

    void Start()
    {
        _sampleCube = new GameObject[_numberOfSamples];

        for (int i = 0; i < _numberOfSamples; i++)
        {
            GameObject cubeIsnt = Instantiate(SampleCubePrefab, transform.position, Quaternion.identity, transform);
            cubeIsnt.name = "SampleCube" + i;
            float angleDivider = 360f / (float)_numberOfSamples;
            transform.eulerAngles = new Vector3(0f, angleDivider * i, 0f);
            cubeIsnt.transform.position = Vector3.forward * 100f;
            _sampleCube[i] = cubeIsnt;
            MeshRenderer meshRenderer = cubeIsnt.transform.GetChild(0).GetComponent<MeshRenderer>();
            Material newMaterial = new Material(meshRenderer.material);
            newMaterial.SetColor("MainColor", _gradientColor.Evaluate(i / (float)_numberOfSamples));
            meshRenderer.material = newMaterial;
        }

        transform.rotation = Quaternion.identity;
        _middleSampleCubes = new GameObject[_quantityOfMiddleCubes];
        float distBetween = 40f;
        float start = transform.position.x - (distBetween * (_quantityOfMiddleCubes - 1) / 2);
        float scale = 12f;

        for (int i = 0; i < _quantityOfMiddleCubes; i++)
        {
            Vector3 pos = transform.position + new Vector3((start + i * distBetween), 0f, 0f);
            GameObject cube = Instantiate(SampleCubePrefab, transform.position, Quaternion.identity, transform);
            MeshRenderer meshRenderer = cube.transform.GetChild(0).GetComponent<MeshRenderer>();
            Material newMaterial = new Material(meshRenderer.material);
            meshRenderer.material = newMaterial;
            cube.name = "SampleMiddleCube" + i;
            _middleSampleCubes[i] = cube;
            cube.transform.position = pos;
            cube.transform.localScale = new Vector3(scale, 1f, scale);
        }

        GUIElementController rotSpeed = _guiController.GetElementController("RotSpeed");
        rotSpeed.SetMinMaxFloat(.1f, 10f);
        GUIElementController GUIMultiLight = _guiController.GetElementController("LightIntensity");
        GUIMultiLight.SetMinMaxFloat(0f, 100);



        _mySettings = new mySettings();
        if (!_mySettings.Settingslst.Where(s => s.className == _nameOfClass).Any())
        {
            _mySettings.Settingslst.Add(new Settingslst { className = _nameOfClass });
            _mySettings.Save();
            _mySettings.Read();
        }

        _thissettings = _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault();

        GUIElementController rotate = _guiController.GetElementController("Rotate");
        GUIElementController clock = _guiController.GetElementController("Clock");
        GUIElementController colorS = _guiController.GetElementController("ColorS");
        GUIElementController colorL = _guiController.GetElementController("ColorL");
        GUIElementController colorMid = _guiController.GetElementController("ColorMid");
        GUIElementController lighInte = _guiController.GetElementController("LightIntensity");
        GUIElementController lColor = _guiController.GetElementController("LightColor");
        
        rotate.SetToggle(_thissettings.rotate_camera);
        clock.SetToggle(_thissettings.clockwise);
        rotSpeed.SetSlider(_thissettings.rotation_speed);
        colorS.SetColorPicker(_thissettings.ColorS);
        colorL.SetColorPicker(_thissettings.ColorL);
        colorMid.SetColorPicker(_thissettings.ColorMid);
        lighInte.SetSlider((float)_thissettings.light_intensity);
        lColor.SetColorPicker(_thissettings.LightColor);
    }

    void Update()
    {
        Savesettings();

        _rotateCamera = _guiController.ReturnValueFrom<bool>("Rotate");
        _rotatingSpeed = _guiController.ReturnValueFrom<float>("RotSpeed");
        _clockwise = _guiController.ReturnValueFrom<bool>("Clock");

        ColorUpdate();
        CameraMovementUpdate();
        ReadFreq();
        BandBufferUpdate();
        //float time = Time.realtimeSinceStartup;
        UpdateBasicSamples();
        UpdateMiddleSamples();

        //time = Time.realtimeSinceStartup - time;
        //Debug.Log("Time: " + time * 1000f + " ms");
    }

    private void Savesettings()
    {
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().clockwise = _clockwise;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().rotate_camera = _rotateCamera;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().rotation_speed = _rotatingSpeed;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().ColorS = _colorF;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().ColorMid = _colorMid;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().ColorL = _colorL;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().light_intensity = _lightIntensityMulit;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().LightColor = _lightColor;
        _mySettings.Save();

    }

    private void UpdateBasicSamples()
    {
        _lightIntensityMulit = _guiController.ReturnValueFrom<float>("LightIntensity");
        float avarage = 0f;

        for (int i = 0; i < _numberOfSamples; i++)
        {
            Transform t = _sampleCube[i].transform;
            Vector3 newScale = new Vector3(t.localScale.x, _bandBuffer[i] * _scaleMulti, t.localScale.z);
            avarage += _bandBuffer[i];
            //SampleCube[i].transform.localScale = Vector3.Lerp(transform.localScale, newScale, Time.deltaTime * _speedOfLerp);
            t.localScale = newScale;
        }

        avarage /= _numberOfSamples;
        _light.intensity = avarage * _lightIntensityMulit * 80000f;
        _light.color = _lightColor;
    }

    private void UpdateMiddleSamples()
    {
        int interval = _numberOfSamples / _quantityOfMiddleCubes;

        for (int i = 0; i < _quantityOfMiddleCubes; i++)
        {
            float avarage = 0f;

            for (int j = i * interval; j < interval * (i + 1); j++)
            {
                avarage += _bandBuffer[j];
            }

            avarage /= interval;
            Transform t = _middleSampleCubes[i].transform;
            Vector3 newScale = new Vector3(t.localScale.x, avarage * _scaleMulti, t.localScale.z);
            t.localScale = newScale;
        }
    }

    private void ReadFreq()
    {
        NativeArray<float> handler = _spectrumAnalyzer.logSpectrumArray;

        for (int i = 0; i < _numberOfSamples; i++)
        {
            _freqBuffer[i] = handler[i];
        }
    }

    private void BandBufferUpdate()
    {
        for (int i = 0; i < _numberOfSamples; i++)
        {
            if (_freqBuffer[i] > _bandBuffer[i])
            {
                _bandBuffer[i] = _freqBuffer[i];
                _bufferDecrease[i] = 0.005f;
            }

            if (_freqBuffer[i] < _bandBuffer[i])
            {
                _bufferDecrease[i] = (_bandBuffer[i] - _freqBuffer[i]) / 8;
                _bandBuffer[i] -= _bufferDecrease[i];
            }
        }
    }

    private void CameraMovementUpdate()
    {
        if (_rotateCamera)
        {
            float speed = _rotatingSpeed * Time.deltaTime * 30f * (_clockwise ? 1f : -1f);
            _cameraParent.Rotate(Vector3.up * speed, Space.World);
        }
    }

    private void ColorUpdate()
    {
        Color getColorF = _guiController.ReturnValueFrom<Color>("ColorS");
        Color getColorL = _guiController.ReturnValueFrom<Color>("ColorL");
        Color getColorMid = _guiController.ReturnValueFrom<Color>("ColorMid");
        _lightColor = _guiController.ReturnValueFrom<Color>("LightColor");

        //if (getColorF != _colorF || getColorL != _colorL)
        //{
        for (int i = 0; i < _numberOfSamples; i++)
        {
            Transform parent = _sampleCube[i].transform;
            Transform tCube = parent.GetChild(0);
            Color handler = Color.Lerp(getColorF, getColorL, (float)i / _numberOfSamples);
            MeshRenderer meshRenderer = tCube.GetComponent<MeshRenderer>();
            float intensity = (parent.localScale.y / 100f) * _maxHDRInten;
            handler *= intensity;
            meshRenderer.material.SetColor("MainColor", handler);
        }

        _colorF = getColorF;
        _colorL = getColorL;
        _colorMid = getColorMid;
        //}

        foreach (GameObject go in _middleSampleCubes)
        {
            MeshRenderer meshRenderer = go.transform.GetChild(0).GetComponent<MeshRenderer>();
            Color c = getColorMid;
            float intensity = (go.transform.localScale.y / 100f) * _maxHDRInten;
            c *= intensity;
            meshRenderer.material.SetColor("MainColor", c);
        }
    }
}
