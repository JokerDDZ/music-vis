using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PanelChooseScene : MonoBehaviour
{
    [SerializeField] private List<SceneInfo> _scenesToChoose = new List<SceneInfo>();
    public GameObject SceneReprPrefab{get => _sceneRepresentPrefab;}
    [SerializeField] private GameObject _sceneRepresentPrefab;
    public GraphicRaycaster GraphicRaycaster{get => _graphicRaycaster;}
    [SerializeField] private GraphicRaycaster _graphicRaycaster;

    private void Awake()
    {
        foreach(SceneInfo si in _scenesToChoose)
        {
            GameObject go = Instantiate(_sceneRepresentPrefab,transform);
            SceneRepresentative sr = go.GetComponent<SceneRepresentative>();
            sr.SetHeader(si.SceneName);
            sr.GUIMode = SceneRepresentative.Mode.ChooseScene;
            sr.SetID = si.SceneID;
            go.GetComponent<Image>().raycastTarget = false;
        }
    }

    public void ExitApp()
    {
        Application.Quit();
    }

    [Serializable]
    public struct SceneInfo
    {
        public int SceneID;
        public string SceneName;
    }
}
