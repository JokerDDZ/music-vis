using System.Collections.Generic;
using UnityEngine;
using Lasp;
using System.Linq;

public class Fractal : MonoBehaviour
{
    [SerializeField] protected GUIController _guiController;
    [SerializeField] protected SpectrumAnalyzer _spectrumAnalyzer;
    [SerializeField] protected Transform _cameraParent;

    protected enum _axis
    {
        XAxis,
        YAxis,
        ZAxis
    };
    [SerializeField]
    protected _axis axis = new _axis();
    protected enum _initiator
    {
        Triangle,
        Square,
        Pentagon,
        Hexagon,
        Heptagon,
        Octagon
    }

    public struct LineSegment
    {
        public Vector3 StartPosition { get; set; }
        public Vector3 EndPosition { get; set; }
        public Vector3 Direction { get; set; }
        public float Length { get; set; }
    }

    [SerializeField]
    protected _initiator initiator = new _initiator();

    [SerializeField]
    protected AnimationCurve _generator;
    [System.Serializable]
    public struct StartGen
    {
        public bool outwards;
        public float scale;
    }
    public StartGen[] _startGen;
    protected Keyframe[] _keys;
    [SerializeField]
    protected bool _useBezierCurves;
    [SerializeField]
    [Range(8, 24)]
    protected int _bezierVertexCount;

    protected int _generationCount;

    protected int _initiatorPointAmount;
    private Vector3[] _initiatorPoint;
    private Vector3 _rotateVector;
    private Vector3 _rotateAxis;

    protected const int _numberOfSamples = 300;

    protected float[] _freqBuffer;
    protected float[] _bandBuffer;
    protected float[] _bufferDecrease;
    private float _initialRotation;
    protected float _rotatingSpeed = 1f;
    protected bool _rotateCamera = false;
    protected bool _clockwise = false;

    [SerializeField]
    protected float _initatorSize;

    protected Vector3[] _position;
    protected Vector3[] _targetPosition;
    protected Vector3[] _bezierPosition;
    private List<LineSegment> _lineSegment;

    #region Settings
    private mySettings _mySettings;
    private Settingslst _thissettings;
    private const string _nameOfClass = "Fractal";
    #endregion

    protected Vector3[] BezierCurve(Vector3[] points, int vertexCount)
    {
        var pointList = new List<Vector3>();
        for (int i = 0; i < points.Length; i += 2)
        {
            if (i + 2 <= points.Length - 1)
            {
                for (float ratio = 0f; ratio <= 1f; ratio += 1.0f / vertexCount)
                {
                    var tangentLineVertex1 = Vector3.Lerp(points[i], points[i + 1], ratio);
                    var tangentLineVertex2 = Vector3.Lerp(points[i + 1], points[i + 2], ratio);
                    var bezierPoint = Vector3.Lerp(tangentLineVertex1, tangentLineVertex2, ratio);
                    pointList.Add(bezierPoint);
                }
            }
        }

        return pointList.ToArray();
    }

    private void Awake()
    {
        _freqBuffer = new float[_numberOfSamples];
        _bandBuffer = new float[_numberOfSamples];
        _bufferDecrease = new float[_numberOfSamples];
        GetInitiatorPoints();
        // assigne list and arrays
        _position = new Vector3[_initiatorPointAmount + 1];
        _targetPosition = new Vector3[_initiatorPointAmount + 1];
        _lineSegment = new List<LineSegment>();
        _keys = _generator.keys;

        _rotateVector = Quaternion.AngleAxis(_initialRotation, _rotateAxis) * _rotateVector;
        for (int i = 0; i < _initiatorPointAmount; i++)
        {
            _position[i] = _rotateVector * _initatorSize;
            _rotateVector = Quaternion.AngleAxis(360 / _initiatorPointAmount, _rotateAxis) * _rotateVector;
        }
        _position[_initiatorPointAmount] = _position[0];
        _targetPosition = _position;

        for (int i = 0; i < _startGen.Length; i++)
        {
            KochGenerator(_targetPosition, _startGen[i].outwards, _startGen[i].scale);
        }
    }

    protected virtual void Start()
    {
        _mySettings = new mySettings();
        if (!_mySettings.Settingslst.Where(s => s.className == _nameOfClass).Any())
        {
            _mySettings.Settingslst.Add(new Settingslst { className = _nameOfClass });
            _mySettings.Save();
            _mySettings.Read();
        }

        _thissettings = _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault();

        GUIElementController rotate = _guiController.GetElementController("Rotate");
        GUIElementController clock = _guiController.GetElementController("Clock");
        GUIElementController rotSpeed = _guiController.GetElementController("RotSpeed");

        rotate.SetToggle(_thissettings.rotate_camera);
        clock.SetToggle(_thissettings.clockwise);
        rotSpeed.SetSlider(_thissettings.rotation_speed);
    }

    protected virtual void Update()
    {
        Savesettings();
    }

    private void Savesettings()
    {
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().clockwise = _clockwise;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().rotate_camera = _rotateCamera;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().rotation_speed = _rotatingSpeed;
        _mySettings.Save();

    }

    protected void KochGenerator(Vector3[] positions, bool outwards, float generatorMultiplier)
    {
        // _lineSegment = new List<LineSegment>();
        _lineSegment.Clear();
        for (int i = 0; i < positions.Length - 1; i++)
        {
            LineSegment line = new LineSegment();
            line.StartPosition = positions[i];
            if (i == positions.Length - 1)
            {
                line.EndPosition = positions[0];
            }
            else
            {
                line.EndPosition = positions[i + 1];

            }
            line.Direction = (line.EndPosition - line.StartPosition).normalized;
            line.Length = Vector3.Distance(line.EndPosition, line.StartPosition);
            _lineSegment.Add(line);
        }
        // add the line segment points to a point array
        List<Vector3> newPos = new List<Vector3>();
        List<Vector3> targetPos = new List<Vector3>();

        for (int i = 0; i < _lineSegment.Count; i++)
        {
            newPos.Add(_lineSegment[i].StartPosition);
            targetPos.Add(_lineSegment[i].StartPosition);

            for (int j = 0; j < _keys.Length - 1; j++)
            {
                float moveAmount = _lineSegment[i].Length * _keys[j].time;
                float heightAmount = (_lineSegment[i].Length * _keys[j].value) * generatorMultiplier;
                Vector3 movePos = _lineSegment[i].StartPosition + (_lineSegment[i].Direction * moveAmount);
                Vector3 Dir;
                if (outwards)
                {
                    Dir = Quaternion.AngleAxis(-90, _rotateAxis) * _lineSegment[i].Direction;
                }
                else
                {
                    Dir = Quaternion.AngleAxis(90, _rotateAxis) * _lineSegment[i].Direction;
                }
                newPos.Add(movePos);
                targetPos.Add(movePos + (Dir * heightAmount));
            }
        }
        newPos.Add(_lineSegment[0].StartPosition);
        targetPos.Add(_lineSegment[0].StartPosition);
        _position = new Vector3[newPos.Count];
        _targetPosition = new Vector3[targetPos.Count];
        _position = newPos.ToArray();
        _targetPosition = targetPos.ToArray();
        _bezierPosition = BezierCurve(_targetPosition, _bezierVertexCount);
        _generationCount++;
    }
    private void OnDrawGizmos()
    {
        GetInitiatorPoints();
        _initiatorPoint = new Vector3[_initiatorPointAmount];

        _rotateVector = Quaternion.AngleAxis(_initialRotation, _rotateAxis) * _rotateVector;
        for (int i = 0; i < _initiatorPointAmount; i++)
        {
            _initiatorPoint[i] = _rotateVector * _initatorSize;
            _rotateVector = Quaternion.AngleAxis(360 / _initiatorPointAmount, _rotateAxis) * _rotateVector;
        }
        for (int i = 0; i < _initiatorPointAmount; i++)
        {
            Gizmos.color = Color.white;
            Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
            Gizmos.matrix = rotationMatrix;

            if (i < _initiatorPointAmount - 1)
            {
                Gizmos.DrawLine(_initiatorPoint[i], _initiatorPoint[i + 1]);
            }
            else
            {
                Gizmos.DrawLine(_initiatorPoint[i], _initiatorPoint[0]);
            }
        }
    }

    private void GetInitiatorPoints()
    {
        switch (initiator)
        {
            case _initiator.Triangle:
                _initialRotation = 0;
                _initiatorPointAmount = 3;
                break;
            case _initiator.Square:
                _initialRotation = 45;

                _initiatorPointAmount = 4;
                break;
            case _initiator.Pentagon:
                _initialRotation = 36;
                _initiatorPointAmount = 5;
                break;
            case _initiator.Hexagon:
                _initialRotation = 30;
                _initiatorPointAmount = 6;
                break;
            case _initiator.Heptagon:
                _initialRotation = 25.71428f;
                _initiatorPointAmount = 7;
                break;
            case _initiator.Octagon:
                _initialRotation = 22.5f;
                _initiatorPointAmount = 8;
                break;
            default:
                _initialRotation = 0;
                _initiatorPointAmount = 3;
                break;
        }
        switch (axis)
        {
            case _axis.XAxis:
                _rotateVector = new Vector3(1, 0, 0);
                _rotateAxis = new Vector3(0, 0, 1);
                break;
            case _axis.YAxis:
                _rotateVector = new Vector3(0, 1, 0);
                _rotateAxis = new Vector3(1, 0, 0);
                break;
            case _axis.ZAxis:
                _rotateVector = new Vector3(0, 0, 1);
                _rotateAxis = new Vector3(0, 1, 0);
                break;
            default:
                _rotateVector = new Vector3(0, 1, 0);
                _rotateAxis = new Vector3(1, 0, 0);
                break;
        }
    }
}
