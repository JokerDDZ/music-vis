using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

sealed class DeviceSelector : MonoBehaviour
{
    public static string DeviceID { get; set; } = null;
    [SerializeField] private InputField _inputField;
    [SerializeField] private PanelQueue _panelQueue;
    [SerializeField] Dropdown _deviceList = null;
    [SerializeField] Button _startButton;
    public bool IsDeviceAcc{get => _isDeviceAccessible;}
    private bool _isDeviceAccessible = false;

    private float _timeLeft = 0f;
    private const float _timer = 1f;

    void Start()
    {
        _deviceList.ClearOptions();
        _deviceList.options.AddRange
        (Lasp.AudioSystem.InputDevices.Select(dev => new DeviceItem(dev)));
        _deviceList.RefreshShownValue();

        if (Lasp.AudioSystem.InputDevices.Any()) OnDeviceSelected(0);

        _startButton.onClick.AddListener(OnStart);
        PanelQueue.Timer = string.IsNullOrEmpty(_inputField.text) ? 0 : int.Parse(_inputField.text);
    }

    private void Update()
    {
        _timeLeft -= Time.deltaTime;

        if (_timeLeft < 0f)
        {
            _isDeviceAccessible = !(Refresh() == 0);
            _timeLeft = _timer;
        }
    }


    public void OnDeviceSelected(int index)
    {
        DeviceID = ((DeviceItem)_deviceList.options[index]).id;
        Debug.Log("Choose device: " + index);
    }

    private void OnStart()
    {
        _panelQueue.GenerateListForMainScene();
        PanelQueue.Timer = string.IsNullOrEmpty(_inputField.text) ? 0 : int.Parse(_inputField.text);
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    private int Refresh()
    {
        var handlerList = Lasp.AudioSystem.InputDevices.Select(dev => new DeviceItem(dev));

        if (handlerList.Count() != _deviceList.options.Count())
        {
            _deviceList.ClearOptions();
            _deviceList.options.AddRange
            (Lasp.AudioSystem.InputDevices.Select(dev => new DeviceItem(dev)));
            _deviceList.RefreshShownValue();

            if (Lasp.AudioSystem.InputDevices.Any()) OnDeviceSelected(0);
        }

        return handlerList.Count();
    }

    class DeviceItem : Dropdown.OptionData
    {
        public string id;
        public DeviceItem(in Lasp.DeviceDescriptor device) => (text, id) = (device.Name, device.ID);
    }
}
