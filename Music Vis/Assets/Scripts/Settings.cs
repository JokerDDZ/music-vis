using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine;


public class mySettings
{
    public List<Settingslst> Settingslst;
    private string saveFile;
    
    public mySettings(){
        saveFile = Application.persistentDataPath + "/mysettings.json";
        //Debug.Log(saveFile);
        Read();
    }

    public void Read()
    {
        //Debug.Log(saveFile+"  exists?");
        if(File.Exists(saveFile))
        {   
            try{
                //Debug.Log("it does");
                string fileContents = File.ReadAllText(saveFile);
                Settingslst[] readsettings = JsonHelper.FromJson<Settingslst>(fileContents);
                Settingslst = new List<Settingslst>();
                Settingslst.AddRange(readsettings);
            }
            catch(Exception e){
                //Debug.LogError("didnt read");
            }
        }else{
            //Debug.Log("doesnt exist");
            //string jsonString = JsonHelper.ToJson(this.Settingslst.ToArray(), true);
            File.WriteAllText(saveFile, "{}");
            try
            {
                //Debug.Log("it does2");
                string fileContents = File.ReadAllText(saveFile);
                Settingslst[] readsettings = JsonHelper.FromJson<Settingslst>(fileContents);
                Settingslst = new List<Settingslst>();
                Settingslst.AddRange(readsettings);
            }
            catch (Exception e)
            {
                //Debug.LogError("didnt read");
            }
        }
    }

    public void Save()
    {
        string jsonString = JsonHelper.ToJson(this.Settingslst.ToArray(), true);
        File.WriteAllText(saveFile, jsonString);
        //Debug.Log("save this: "+jsonString);

    }
}

[Serializable]
public class Settingslst{
    public string className;
    public bool rotate_camera;
    public bool clockwise;
    public float rotation_speed;
    public double light_intensity;
    public Color ColorS;
    public Color ColorL;
    public Color ColorMid;
    public Color LightColor;
}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }
    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}