using Unity.Collections;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(LineRenderer))]
public class KochLine : Fractal
{
  LineRenderer _lineRenderer;

  Vector3[] _lerpPosition;
  public float _generateMultiplier;
  private float _amplitude = 0f;
  private float _amplitudeHighest = 0f;


  public int _audioBand;
  // Start is called before the first frame update

  void Start()
  {
    _lineRenderer = GetComponent<LineRenderer>();
    _lineRenderer.enabled = true;
    _lineRenderer.useWorldSpace = false;
    _lineRenderer.loop = true;
    _lineRenderer.positionCount = _position.Length;
    _lineRenderer.SetPositions(_position);
    _lerpPosition = new Vector3[_position.Length];

  }

  void GetAmplitude()
  {
    float boostLowValue = 0f;
    float currentAmplitude = 0;
    for (int i = 0; i < 300; i++)
    {
      currentAmplitude += _bandBuffer[i];
      if (i < 10 && _bandBuffer[i] >= .7f)
      {
        boostLowValue += _bandBuffer[i];
      }
    }
    if (currentAmplitude > _amplitudeHighest)
    {
      _amplitudeHighest = currentAmplitude;
    }

    _amplitude = (currentAmplitude / _amplitudeHighest) + (boostLowValue / 20f);
  }
  void Update()
  {
    ReadFreq();
    BandBufferUpdate();
    GetAmplitude();

    if (_generationCount != 0)
    {
      for (int i = 0; i < _position.Length; i++)
      {
        _lerpPosition[i] = Vector3.Lerp(_position[i], _targetPosition[i], _amplitude);
      }
      if (_useBezierCurves)
      {
        _bezierPosition = BezierCurve(_lerpPosition, _bezierVertexCount);
        _lineRenderer.positionCount = _bezierPosition.Length;
        _lineRenderer.SetPositions(_bezierPosition);
      }
      else
      {
        _lineRenderer.positionCount = _lerpPosition.Length;
        _lineRenderer.SetPositions(_lerpPosition);

      }
    }
  }

  private void ReadFreq()
  {
    NativeArray<float> handler = _spectrumAnalyzer.logSpectrumArray;

    for (int i = 0; i < _numberOfSamples; i++)
    {
      _freqBuffer[i] = handler[i];
    }
  }

  private void BandBufferUpdate()
  {
    for (int i = 0; i < _numberOfSamples; i++)
    {
      if (_freqBuffer[i] > _bandBuffer[i])
      {
        _bandBuffer[i] = _freqBuffer[i];
        _bufferDecrease[i] = 0.005f;
      }

      if (_freqBuffer[i] < _bandBuffer[i])
      {
        _bufferDecrease[i] = (_bandBuffer[i] - _freqBuffer[i]) / 8;
        _bandBuffer[i] -= _bufferDecrease[i];
      }
    }
  }
}
