using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

public class KochTrails : Fractal
{

  public class TrailObject
  {
    public GameObject GO;
    public TrailRenderer Trail;
    public int CurrentTargetNum;
    public Vector3 TargetPosition;
    public Color EmisssionColor;
  }

  [HideInInspector]
  public List<TrailObject> trails;

  [Header("Trail Properties")]
  public GameObject trailPrefab;
  public AnimationCurve trailWidthCurve;
  [Range(0, 8)]
  public int trailEndCapVertices;
  public Material trailMaterial;
  public Gradient trailColor;
  private GradientColorKey[] colorKey;
  private GradientAlphaKey[] alphaKey;

  [Header("Audio")]
  public Vector2 speedMinMax, widthMinMax, trailTimeMinMax;
  public float colorMultiplier;

  private float _lerpPosSpeed;
  private float _distanceSnap;
  private Color _startColor, _endColor;


  private float _amplitude = 0f;
  private float _amplitudeHighest = 0f;

  protected override void Start()
  {
    base.Start();
    GUIElementController rotSpeed = _guiController.GetElementController("RotSpeed");
    rotSpeed.SetMinMaxFloat(.1f, 10f);
    trails = new List<TrailObject>();
    _startColor = new Color(0, 0, 0, 0);
    _endColor = new Color(0, 0, 0, 1);
    for (int i = 0; i < _initiatorPointAmount; i++)
    {
      GameObject trailInstance = Instantiate(trailPrefab, transform.position, Quaternion.identity, transform);
      TrailObject trailObjectInstance = new TrailObject
      {
        GO = trailInstance,
        Trail = trailInstance.GetComponent<TrailRenderer>(),
        EmisssionColor = trailColor.Evaluate(i * (1.0f / _initiatorPointAmount))
      };

      trailObjectInstance.Trail.material = new Material(trailMaterial);
      trailObjectInstance.Trail.numCapVertices = trailEndCapVertices;
      trailObjectInstance.Trail.widthCurve = trailWidthCurve;
      Vector3 instantiatePosition;
      if (_generationCount > 0)
      {
        int step;
        if (_useBezierCurves)
        {
          step = _bezierPosition.Length / _initiatorPointAmount;
          instantiatePosition = _bezierPosition[i * step];
          trailObjectInstance.CurrentTargetNum = (i * step) + 1;
          trailObjectInstance.TargetPosition = _bezierPosition[trailObjectInstance.CurrentTargetNum];

        }
        else
        {
          step = _position.Length / _initiatorPointAmount;
          instantiatePosition = _position[i * step];
          trailObjectInstance.CurrentTargetNum = (i * step) + 1;
          trailObjectInstance.TargetPosition = _position[trailObjectInstance.CurrentTargetNum];

        }
      }
      else
      {
        instantiatePosition = _position[i];
        trailObjectInstance.CurrentTargetNum = i + 1;
        trailObjectInstance.TargetPosition = _position[trailObjectInstance.CurrentTargetNum];
      }
      trailObjectInstance.GO.transform.localPosition = instantiatePosition;
      trails.Add(trailObjectInstance);
    }
  }

  protected override void Update()
  {
    base.Update();
    _rotateCamera = _guiController.ReturnValueFrom<bool>("Rotate");
    _rotatingSpeed = _guiController.ReturnValueFrom<float>("RotSpeed");
    _clockwise = _guiController.ReturnValueFrom<bool>("Clock");
    CameraMovementUpdate();
    ReadFreq();
    BandBufferUpdate();
    GetAmplitude();
    Movement();
    AudioBehavior();
  }

  private void Movement()
  {
    _lerpPosSpeed = Mathf.Lerp(speedMinMax.x, speedMinMax.y, _amplitude);
    for (int i = 0; i < trails.Count; i++)
    {
      _distanceSnap = Vector3.Distance(trails[i].GO.transform.localPosition, trails[i].TargetPosition);
      if (_distanceSnap < 0.05f)
      {
        trails[i].GO.transform.localPosition = trails[i].TargetPosition;
        if (_useBezierCurves && _generationCount > 0)
        {
          if (trails[i].CurrentTargetNum < _bezierPosition.Length - 1)
          {
            trails[i].CurrentTargetNum++;
          }
          else
          {
            trails[i].CurrentTargetNum = 1;

          }
          trails[i].TargetPosition = _bezierPosition[trails[i].CurrentTargetNum];
        }
        else
        {
          if (trails[i].CurrentTargetNum < _position.Length - 1)
          {
            trails[i].CurrentTargetNum++;
          }
          else
          {
            trails[i].CurrentTargetNum = 1;
          }
          trails[i].TargetPosition = _targetPosition[trails[i].CurrentTargetNum];
        }
      }
      trails[i].GO.transform.localPosition = Vector3.MoveTowards(trails[i].GO.transform.localPosition, trails[i].TargetPosition, Time.deltaTime * _lerpPosSpeed);
    }
  }

  private void CameraMovementUpdate()
  {
    if (_rotateCamera)
    {
      float speed = _rotatingSpeed * Time.deltaTime * 30f * (_clockwise ? 1f : -1f);
      _cameraParent.Rotate(Vector3.up * speed, Space.World);
    }
  }


  private void AudioBehavior()
  {
    for (int i = 0; i < _initiatorPointAmount; i++)
    {
      Color colorLerp = Color.Lerp(_startColor, trails[i].EmisssionColor * colorMultiplier, _amplitude * 10f);
      trails[i].Trail.material.SetColor("_BaseColor", colorLerp);
      // trails[i].Trail.material.color = colorLerp;

      colorLerp = Color.Lerp(_startColor, _endColor, .5f);
      trails[i].Trail.material.SetColor("_EmissionColor", colorLerp);
      // trails[i].Trail.material.EnableKeyword("_EMISSION");

      float widthLerp = Mathf.Lerp(widthMinMax.x, widthMinMax.y, _bandBuffer[i]);
      trails[i].Trail.widthMultiplier = widthLerp;

      float timeLerp = Mathf.Lerp(trailTimeMinMax.x, trailTimeMinMax.y, _bandBuffer[i]);
      trails[i].Trail.time = timeLerp;

    }
  }

  void GetAmplitude()
  {
    float boostLowValue = 0f;
    float currentAmplitude = 0;
    for (int i = 0; i < 300; i++)
    {
      currentAmplitude += _bandBuffer[i];
      if (i < 10 && _bandBuffer[i] >= .7f)
      {
        boostLowValue += _bandBuffer[i];
      }
    }
    if (currentAmplitude > _amplitudeHighest)
    {
      _amplitudeHighest = currentAmplitude;
    }

    _amplitude = (currentAmplitude / _amplitudeHighest);
  }

  private void ReadFreq()
  {
    NativeArray<float> handler = _spectrumAnalyzer.logSpectrumArray;

    for (int i = 0; i < _numberOfSamples; i++)
    {
      _freqBuffer[i] = handler[i];
    }
  }

  private void BandBufferUpdate()
  {
    for (int i = 0; i < _numberOfSamples; i++)
    {
      if (_freqBuffer[i] > _bandBuffer[i])
      {
        _bandBuffer[i] = _freqBuffer[i];
        _bufferDecrease[i] = 0.005f;
      }

      if (_freqBuffer[i] < _bandBuffer[i])
      {
        _bufferDecrease[i] = (_bandBuffer[i] - _freqBuffer[i]) / 8;
        _bandBuffer[i] -= _bufferDecrease[i];
      }
    }
  }
}
