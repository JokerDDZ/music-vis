using UnityEngine;
using System.Linq;
using Unity.Collections;
using Lasp;

public class HexagonHandler : MonoBehaviour
{
    private const int _numberOfSamples = 91;
    private const int _soundres = 512;
    private float[] _freqBuffer;
    private float[] _bandBuffer;
    private float[] _bufferDecrease;
    Settingslst thissettings;
    mySettings mySettings;
    [SerializeField] private GUIController _guiController;
    [SerializeField] private Transform _cameraParent;
    [SerializeField] private Light _light;
    [SerializeField] public GameObject hexagonsprefab;
    private GameObject[] _sampleCube;
    private GameObject[] _middleSampleCubes;
    [SerializeField] private SpectrumAnalyzer _spectrumAnalyzer;
    [SerializeField] private float _scaleMulti = 100f;
    [SerializeField] private float _speedOfLerp = 5f;
    private const int _quantityOfMiddleCubes = 4;
    private float _maxHDRInten = 8f;
    private float _lightIntensityMulit = 1000f;

    // Start is called before the first frame update
    private void Awake()
    {
        _freqBuffer = new float[_soundres];
        _bandBuffer = new float[_soundres];
        _bufferDecrease = new float[_soundres];
    }

    void Start()
    {
        ///
        ///settings segment start
        ///clasa mysettings zawiera zapisywanie itp a settingslst to jest obiekt z ktorego poieramy ustawienia dla danej sceny
        ///
        mySettings = new mySettings();
        if (!mySettings.Settingslst.Where(s => s.className == "Hexagon").Any())
        {
            mySettings.Settingslst.Add(new Settingslst { className = "Hexagon" });
            mySettings.Save();
            mySettings.Read();
        }
        thissettings = mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault();

        //tutaj probowalem ustawic slidery i colorpicker
        //toggle dziala bez problemu reszta nie
        GUIElementController GUIMultiLight = _guiController.GetElementController("LightIntensity");
        GUIMultiLight.SetMinMaxFloat(0f, 100f);
        GUIElementController Rotate = _guiController.GetElementController("Rotate");       
        GUIElementController Clock = _guiController.GetElementController("Clock");
        GUIElementController rotSpeed = _guiController.GetElementController("RotSpeed");
        rotSpeed.SetMinMaxFloat(.1f, 10f);
        GUIElementController colorS = _guiController.GetElementController("ColorS");
        GUIElementController colorL = _guiController.GetElementController("ColorL");
        GUIElementController colorM = _guiController.GetElementController("ColorMid");
        GUIElementController lightColor = _guiController.GetElementController("LightColor");

        #region Set from JSON


        colorS.SetColorPicker(thissettings.ColorS);
        colorL.SetColorPicker(thissettings.ColorL);
        colorM.SetColorPicker(thissettings.ColorMid);
        lightColor.SetColorPicker(thissettings.LightColor);

        GUIMultiLight.SetSlider((float)thissettings.light_intensity);
        rotSpeed.SetSlider((float)thissettings.rotation_speed);

        Clock.SetToggle(thissettings.clockwise);
        Rotate.SetToggle(thissettings.rotate_camera);
        #endregion

        _sampleCube = new GameObject[_numberOfSamples];

        for (int i = 0; i < _numberOfSamples; i++)
        {
            GameObject cubeIsnt = Instantiate(hexagonsprefab.transform.GetChild(i).gameObject, hexagonsprefab.transform.GetChild(i).gameObject.transform.position, Quaternion.identity, transform);
            cubeIsnt.transform.Rotate(90.0f, 0.0f, 0.0f, Space.Self);
            cubeIsnt.name = "SampleCube" + i;
            _sampleCube[i] = cubeIsnt;
            MeshRenderer meshRenderer = cubeIsnt.GetComponent<MeshRenderer>();
            Material newMaterial = new Material(meshRenderer.material);
            meshRenderer.material = newMaterial;
        }

        hexagonsprefab.active = false;
        transform.rotation = Quaternion.identity;

        ColorUpdate();
        CameraMovementUpdate();
    }


    // Update is called once per frame
    void Update()
    {
        thissettings.rotate_camera = _guiController.ReturnValueFrom<bool>("Rotate");
        thissettings.rotation_speed = _guiController.ReturnValueFrom<float>("RotSpeed");
        thissettings.clockwise = _guiController.ReturnValueFrom<bool>("Clock");
        thissettings.light_intensity = _guiController.ReturnValueFrom<float>("LightIntensity");

        ReadFreq();
        BandBufferUpdate();
        UpdateMiddleSamples();
        ColorUpdate();
        CameraMovementUpdate();
        ///zapisywanie
        savesettings();
    }

    void savesettings()
    {
        //saving all data to json

        mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault().clockwise = thissettings.clockwise;
        mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault().LightColor = thissettings.LightColor;
        mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault().light_intensity = thissettings.light_intensity;
        mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault().rotate_camera = thissettings.rotate_camera;
        mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault().rotation_speed = thissettings.rotation_speed;
        mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault().ColorL = thissettings.ColorL;
        mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault().ColorMid = thissettings.ColorMid;
        mySettings.Settingslst.Where(s => s.className == "Hexagon").FirstOrDefault().ColorS = thissettings.ColorS;

        mySettings.Save();

    }
    
    private void ReadFreq()
    {
        NativeArray<float> handler = _spectrumAnalyzer.logSpectrumArray;

        for (int i = 0; i < _soundres; i++)
        {
            _freqBuffer[i] = handler[i];
        }
    }
    private void BandBufferUpdate()
    {
        for (int i = 0; i < _soundres; i++)
        {
            if (_freqBuffer[i] > _bandBuffer[i])
            {
                _bandBuffer[i] = _freqBuffer[i];
                _bufferDecrease[i] = 0.005f;
            }

            if (_freqBuffer[i] < _bandBuffer[i])
            {
                _bufferDecrease[i] = (_bandBuffer[i] - _freqBuffer[i]) / 8;
                _bandBuffer[i] -= _bufferDecrease[i];
            }
        }
    }


    private void UpdateMiddleSamples()
    {
        int interval = _soundres / _numberOfSamples;
        float allavg = 0f;
        for (int i = 0; i < _numberOfSamples; i++)
        {
            float avarage = 0f;

            for (int j = i * interval; j < interval * (i + 1); j++)
            {
                avarage += _bandBuffer[j];
            }

            avarage /= interval;
            Transform t = _sampleCube[i].transform;
            Vector3 newScale = new Vector3(t.localScale.x, t.localScale.y, -1 + (-avarage * 10f));
            allavg += avarage;
            t.localScale = newScale;
        }
        allavg /= _numberOfSamples;
        _light.intensity = allavg * _lightIntensityMulit * 80000f;
        _light.color = thissettings.LightColor;
    }

    private void CameraMovementUpdate()
    {
        if (thissettings.rotate_camera)
        {
            float speed = thissettings.rotation_speed * Time.deltaTime * 30f * (thissettings.clockwise ? 1f : -1f);
            _cameraParent.Rotate(Vector3.up * speed, Space.World);
        }
    }

    private void ColorUpdate()
    {
        thissettings.ColorS = _guiController.ReturnValueFrom<Color>("ColorS");
        thissettings.ColorL = _guiController.ReturnValueFrom<Color>("ColorL");
        thissettings.ColorMid = _guiController.ReturnValueFrom<Color>("ColorMid");
        thissettings.LightColor = _guiController.ReturnValueFrom<Color>("LightColor");

        for (int i = 0; i < _numberOfSamples; i++)
        {
            Transform parent = _sampleCube[i].transform;
            var temp = parent.childCount;
            Transform tCube = parent;
            Color handler = Color.Lerp(thissettings.ColorS, thissettings.ColorL, (float)i / _numberOfSamples);
            MeshRenderer meshRenderer = tCube.GetComponent<MeshRenderer>();
            float intensity = 2;
            handler *= intensity;
            meshRenderer.material.SetColor("MainColor", handler);
        }
    }
}
