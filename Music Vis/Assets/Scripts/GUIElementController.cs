using UnityEngine;
using UnityEngine.UI;
using System;

public class GUIElementController : MonoBehaviour
{
    public string SetHeader { set => _header.text = value; }
    [SerializeField] private Text _header;
    [Header("Update objects")]
    [SerializeField] private Toggle _boolToggle;
    private Color _colorReturnValue = Color.white;
    private bool _boolReturnValue = false;
    #region float
    [SerializeField] private Slider _floatSlider;
    [SerializeField] private Text _floatText;
    [SerializeField] private Text _minText;
    [SerializeField] private Text _maxText;
    private float _floatReturnValue = 0f;
    private float _min = 0f;
    private float _max = 100f;
    #endregion

    #region Color
    [SerializeField] private ColorPicker _colorPicker;
    #endregion

    public string SetID { set => _id = value; }
    public string GetID { get => _id; }
    private string _id { get; set; }

    private void Update()
    {
        UpdateBool();
        UpdateFloat();
    }

    private void UpdateBool()
    {
        if (!_boolToggle) return;

        _boolReturnValue = _boolToggle.isOn;
    }

    private void UpdateFloat()
    {
        if (!_floatText) return;

        _floatReturnValue = Mathf.Lerp(_min, _max, _floatSlider.value);
        _floatReturnValue = Mathf.Clamp(_floatReturnValue, _min, _max);
        _floatText.text = String.Format("{0:0.00}", _floatReturnValue);
    }

    public void SetToggle(bool b)
    {
        _boolToggle.isOn = b;
    }

    public void SetSlider(float f)
    {
        _floatSlider.value = f/_max;
    }

    public void SetColor(Color c)
    {
        _colorReturnValue = c;
    }

    public void SetColorPicker(Color c)
    {
        _colorPicker.SetActualColor(c);
    }

    public void SetMinMaxFloat(float min, float max)
    {
        _min = min;
        _max = max;
        _minText.text = _min.ToString();
        _maxText.text = _max.ToString();
    }

    public dynamic getValue<T>()
    {
        if (typeof(T) == typeof(bool))
        {
            return _boolReturnValue;
        }
        else if (typeof(T) == typeof(float))
        {
            return _floatReturnValue;
        }
        else if (typeof(T) == typeof(Color))
        {
            return _colorReturnValue;
        }
        else
        {
            Debug.LogError("Wrong type");
            return "Error";
        }
    }

    private void OnEnable()
    {
        if (!_boolToggle) return;

        _boolToggle.isOn = _boolReturnValue;
    }

    private void OnDisable()
    {
        if (!_boolToggle) return;

        _boolToggle.isOn = _boolReturnValue;
    }
}
