using UnityEngine;
using Lasp;
using System.Collections.Generic;

public class AudioSceneManager : MonoBehaviour
{
    [SerializeField] private SpectrumAnalyzer _spectrumAnalyzer;
    [SerializeField] private List<AudioLevelTracker> _audioLeveltrackers;

    private void Awake()
    {
        if (_spectrumAnalyzer)
        {
            if (DeviceSelector.DeviceID != null)
            {
                _spectrumAnalyzer.useDefaultDevice = false;
                _spectrumAnalyzer.deviceID = DeviceSelector.DeviceID;
            }
            else
            {
                _spectrumAnalyzer.useDefaultDevice = true;
            }
        }
        else
        {
            foreach (AudioLevelTracker at in _audioLeveltrackers)
            {
                if (DeviceSelector.DeviceID != null)
                {
                    at.useDefaultDevice = false;
                    at.deviceID = DeviceSelector.DeviceID;
                }
                else
                {
                    at.useDefaultDevice = true;
                }
            }
        }
    }
}
