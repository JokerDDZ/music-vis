using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class MainSceneLoader : MonoBehaviour
{
    private List<PanelChooseScene.SceneInfo> _sceneInfo = new List<PanelChooseScene.SceneInfo>();
    private List<int> _alreadyLoaded = new List<int>();
    private int _currID = 0;
    private float _timer = 0f;
    private float _timeLeft = 0f;

    private void Start()
    {
        _sceneInfo = PanelQueue.SceneInfos;
        SceneManager.LoadScene(_sceneInfo[0].SceneID, LoadSceneMode.Additive);
        ActivateSceneIndex(1);
        _timer = PanelQueue.Timer;
        _timeLeft = _timer;
        _currID = 0;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            LoadNextScene();
        }

        if (_timer > 0)
        {
            _timeLeft -= Time.deltaTime;

            if (_timeLeft < 0f)
            {
                LoadNextScene();
                _timeLeft = _timer;
            }
        }
    }

    private void LoadNextScene()
    {
        int count = _sceneInfo.Count;
        int oldID = _currID;
        _currID++;

        if (_currID >= count)
        {
            _currID = 0;
        }

        if (_sceneInfo[oldID].SceneID != _sceneInfo[_currID].SceneID)
        {
            SceneManager.UnloadSceneAsync(_sceneInfo[oldID].SceneID);
            SceneManager.LoadScene(_sceneInfo[_currID].SceneID, LoadSceneMode.Additive);
            ActivateSceneIndex(1);
        }
    }

    private void ActivateSceneIndex(int index)
    {
        StopAllCoroutines();
        StartCoroutine(DelayActivate(index));
    }

    private IEnumerator DelayActivate(int index)
    {
        bool loaded = false;
        Scene sceneHand = new Scene();

        while (!loaded)
        {
            yield return 0;
            sceneHand = SceneManager.GetSceneAt(index);
            Debug.Log("Scnene at index: " + index + " name: " + sceneHand.name);
            loaded = sceneHand.isLoaded;
        }

        SceneManager.SetActiveScene(sceneHand);
        Debug.Log("Activate scene: " + sceneHand.name);
    }
}
