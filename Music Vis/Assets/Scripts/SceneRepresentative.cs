using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class SceneRepresentative : MonoBehaviour, IPointerDownHandler
{
    public enum Mode
    {
        ChooseScene,
        Queue
    }

    private GraphicRaycaster _graphicRaycaster;
    private EventSystem _eventSystem;
    public Mode GUIMode = Mode.ChooseScene;
    public string GetHeader{get => _text.text;}
    public bool Held { set => _held = value; }
    private bool _held = false;
    public int GetID {get => _id;}
    public int SetID {set => _id = value;}
    private int _id = -1;

    [SerializeField] private Text _text;

    private void Start()
    {
        PanelChooseScene panelToChooseScene = GameObject.FindObjectOfType<PanelChooseScene>();
        _eventSystem = GameObject.FindObjectOfType<EventSystem>();
        _graphicRaycaster = panelToChooseScene.GraphicRaycaster;
    }

    private void Update()
    {
        if (_held)
        {
            if (Input.GetMouseButtonUp(0))
            {
                PointerEventData eventData = new PointerEventData(_eventSystem);
                eventData.position = Input.mousePosition;
                List<RaycastResult> results = new List<RaycastResult>();
                _graphicRaycaster.Raycast(eventData, results);
                PanelQueue pq = null;

                foreach (RaycastResult result in results)
                {
                    pq = result.gameObject.GetComponent<PanelQueue>();
                    if (pq != null) break;
                }


                if(pq != null)
                {
                    pq.AddScene(this);
                }

                Destroy(gameObject);
            }

            transform.position = Input.mousePosition;
        }
    }

    public void SetHeader(string s)
    {
        _text.text = s;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        switch (GUIMode)
        {
            case Mode.ChooseScene:
                {
                    GameObject go = Instantiate(gameObject, transform.parent.parent);
                    SceneRepresentative sr = go.GetComponent<SceneRepresentative>();
                    sr.SetID = _id;
                    sr.Held = true;
                }
                break;
            case Mode.Queue:
                {
                    _held = true;
                    transform.parent = GameObject.Find("Canvas").transform;
                }
                break;
        }
    }
}
