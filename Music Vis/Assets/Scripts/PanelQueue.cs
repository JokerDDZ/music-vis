using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class PanelQueue : MonoBehaviour
{
    public static int Timer;
    public static List<PanelChooseScene.SceneInfo> SceneInfos;
    [SerializeField] private PanelChooseScene _panelChooseScene;
    [SerializeField] private Transform _panelTransform;
    [SerializeField] private Button _startButton;
    [SerializeField] private DeviceSelector _deviceSelector;

    private void Update()
    {
        _startButton.interactable = !(_panelTransform.childCount == 0) && _deviceSelector.IsDeviceAcc;
    }

    public void AddScene(SceneRepresentative sr)
    {
        GameObject go = Instantiate(_panelChooseScene.SceneReprPrefab, _panelTransform);
        SceneRepresentative srHandler = go.GetComponent<SceneRepresentative>();
        srHandler.GUIMode = SceneRepresentative.Mode.Queue;
        srHandler.SetID = sr.GetID;
        srHandler.SetHeader(sr.GetHeader);
    }

    public void GenerateListForMainScene()
    {
        PanelQueue.SceneInfos = new List<PanelChooseScene.SceneInfo>();

        foreach (Transform t in _panelTransform)
        {
            SceneRepresentative sr = t.GetComponent<SceneRepresentative>();
            PanelQueue.SceneInfos.Add(new PanelChooseScene.SceneInfo() { SceneID = sr.GetID, SceneName = null });
        }
    }
}
