using UnityEngine;
using Unity.Collections;
using Lasp;
using System.Linq;

public class PlaneMeshDeform : MonoBehaviour
{
    [SerializeField] private SpectrumAnalyzer _spectrumAnalyzer;
    [SerializeField] private GUIController _guiController;
    [SerializeField] Transform _cameraParent;
    [SerializeField] private MeshRenderer _floorMeshRenderer;
    private MeshRenderer _renderer;
    private ComputeBuffer _vertexBuffer;
    private ComputeBuffer _initialBuffer;
    private Mesh _mesh;
    private int _numberOfSamples = 300;
    private float[] _freqBuffer;
    private float[] _bandBuffer;
    private float[] _bufferDecrease;
    private Vector3[] _vertices;
    private int[] _triangles;
    [SerializeField] private int _sizeX = 20;
    [SerializeField] private int _sizeZ = 20;
    [SerializeField] private float _density = .5f;

    #region Settings
    private mySettings _mySettings;
    private Settingslst _thissettings;
    private const string _nameOfClass = "PlaneDeform";
    #endregion

    private void Awake()
    {
        _freqBuffer = new float[_numberOfSamples];
        _bandBuffer = new float[_numberOfSamples];
        _bufferDecrease = new float[_numberOfSamples];
        _mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = _mesh;
        _renderer = GetComponent<MeshRenderer>();
        _renderer.material = new Material(_renderer.material);
        _floorMeshRenderer.material = new Material(_floorMeshRenderer.material);
        CreateShape();
    }

    void Start()
    {
        InitData();
        GUIElementController rotSpeed = _guiController.GetElementController("Speed");
        rotSpeed.SetMinMaxFloat(.1f, 10f);


        _mySettings = new mySettings();
        if (!_mySettings.Settingslst.Where(s => s.className == _nameOfClass).Any())
        {
            _mySettings.Settingslst.Add(new Settingslst { className = _nameOfClass });
            _mySettings.Save();
            _mySettings.Read();
        }

        _thissettings = _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault();

        GUIElementController rotate = _guiController.GetElementController("Rotate");
        GUIElementController clock = _guiController.GetElementController("Clockwise");
        GUIElementController colorL = _guiController.GetElementController("ColorL");
        GUIElementController colorH = _guiController.GetElementController("ColorH");

        rotate.SetToggle(_thissettings.rotate_camera);
        clock.SetToggle(_thissettings.clockwise);
        rotSpeed.SetSlider(_thissettings.rotation_speed);
        colorL.SetColorPicker(_thissettings.ColorL);
        colorH.SetColorPicker(_thissettings.ColorS);
    }

    void Update()
    {
        bool rotate = _guiController.ReturnValueFrom<bool>("Rotate");
        bool clockWise = _guiController.ReturnValueFrom<bool>("Clockwise");
        float rotatingSpeed = _guiController.ReturnValueFrom<float>("Speed");
        Color colorH = _guiController.ReturnValueFrom<Color>("ColorH");
        Color colorL = _guiController.ReturnValueFrom<Color>("ColorL");
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().ColorL = colorL;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().ColorS = colorH;
        colorH *= 2f;
        colorL *= 2f;
        _renderer.material.SetColor("_highColor", colorH);
        _renderer.material.SetColor("_lowColor", colorL);
        _floorMeshRenderer.material.SetColor("_highColor", colorH);
        _floorMeshRenderer.material.SetColor("_lowColor", colorL);

        if (rotate)
        {
            float speed = rotatingSpeed * Time.deltaTime * 30f * (clockWise ? 1f : -1f);
            _cameraParent.Rotate(Vector3.up * speed, Space.World);
        }

        ReadFreq();
        BandBufferUpdate();
        UpdateMesh();


        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().clockwise = clockWise;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().rotate_camera = rotate;
        _mySettings.Settingslst.Where(s => s.className == _nameOfClass).FirstOrDefault().rotation_speed = rotatingSpeed;

        _mySettings.Save();
    }

    private void CreateShape()
    {
        _vertices = new Vector3[(_sizeX + 1) * (_sizeZ + 1)];

        int index = 0;

        for (int i = 0; i <= _sizeZ; i++)
        {
            for (int j = 0; j <= _sizeX; j++)
            {
                _vertices[index] = new Vector3(j * _density, 0, i * _density);
                index++;
            }
        }

        _triangles = new int[_sizeX * _sizeZ * 6];
        int vert = 0;
        int tris = 0;

        for (int j = 0; j < _sizeZ; j++)
        {
            for (int i = 0; i < _sizeX; i++)
            {
                _triangles[tris] = vert;
                _triangles[tris + 1] = vert + _sizeX + 1;
                _triangles[tris + 2] = vert + 1;
                _triangles[tris + 3] = vert + 1;
                _triangles[tris + 4] = vert + _sizeX + 1;
                _triangles[tris + 5] = vert + _sizeX + 2;

                vert++;
                tris += 6;
            }

            vert++;
        }
    }

    private void UpdatePosOfVertices()
    {
        int pointsXToUpdate = 100;
        int interval = _numberOfSamples / pointsXToUpdate;
        float[] avg = new float[pointsXToUpdate];

        for (int i = 0; i < avg.Length; i++)
        {
            float avarage = 0f;

            for (int j = i * interval; j < interval * (i + 1); j++)
            {
                avarage += _bandBuffer[j];
            }

            avarage /= interval;
            avg[i] = avarage;
        }

        int offsetX = 5;
        int offsetZ = 5;

        for (int j = 21; j >= 2; j--)
        {
            for (int i = 0; i < avg.Length; i++)
            {
                int index = i + offsetX + (offsetZ * j * _sizeZ) + offsetZ * j;
                int index2 = i + offsetX + (offsetZ * (j - 1) * _sizeZ) + offsetZ * (j - 1);
                Vector3 hand = _vertices[index];
                Vector3 hand2 = _vertices[index2];
                _vertices[index] = new Vector3(hand.x, hand2.y, hand.z);
            }
        }

        for (int i = 0; i < avg.Length; i++)
        {
            int index = i + offsetX + (offsetZ * _sizeZ) + offsetZ;
            Vector3 hand = _vertices[index];
            _vertices[index] = new Vector3(hand.x, avg[i], hand.z);
        }
    }

    private void InitData()
    {
        MeshFilter mf = GetComponent<MeshFilter>();
        _mesh = mf.mesh;
    }

    private void ReadFreq()
    {
        NativeArray<float> handler = _spectrumAnalyzer.logSpectrumArray;

        for (int i = 0; i < _numberOfSamples; i++)
        {
            _freqBuffer[i] = handler[i];
        }
    }

    private void BandBufferUpdate()
    {
        for (int i = 0; i < _numberOfSamples; i++)
        {
            if (_freqBuffer[i] > _bandBuffer[i])
            {
                _bandBuffer[i] = _freqBuffer[i];
                _bufferDecrease[i] = 0.005f;
            }

            if (_freqBuffer[i] < _bandBuffer[i])
            {
                _bufferDecrease[i] = (_bandBuffer[i] - _freqBuffer[i]) / 8;
                _bandBuffer[i] -= _bufferDecrease[i];
            }
        }
    }

    private void UpdateMesh()
    {
        _mesh.Clear();
        UpdatePosOfVertices();
        _mesh.vertices = _vertices;
        _mesh.triangles = _triangles;
        _mesh.RecalculateNormals();
    }
}
