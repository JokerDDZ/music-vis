using UnityEngine;
using System.Linq;

public class ParticlesCompute2D : MonoBehaviour
{
    [SerializeField] private ComputeShader _computeShader;
    private ComputeBuffer _particlesBuffer;
    [SerializeField] private Material _material;
    private int _pointSize = 2;
    private const int SIZEPARTICLES = 7 * sizeof(float);
    [SerializeField] private int _numberOfParticles = 10000;
    public float SetCurrRange { set => _currRange = value; }
    [Range(0, 100)] [SerializeField] private float _currRange = 0f;
    public Color SetColor { set => _color = value; }
    private Color _color;
    private int _kernelID = -1;
    private int _groupSizeX = 0;

    void Start()
    {
        Init();
    }

    void Update()
    {  
        _computeShader.SetFloat("currentRange", _currRange);
        _computeShader.SetFloat("deltaTime", Time.deltaTime);
        _computeShader.SetVector("center", transform.position);
        _computeShader.Dispatch(_kernelID, _groupSizeX, 1, 1);
        _material.SetFloat("Distance", _currRange);
        _material.SetColor("BaseColor", _color);
    }



    private void OnRenderObject()
    {
        _material.SetPass(0);
        Graphics.DrawProceduralNow(MeshTopology.Points, 1, _numberOfParticles);
    }

    private void Init()
    {
        _computeShader = (ComputeShader)Instantiate(_computeShader);
        _material = new Material(_material);
        Particle[] particles = new Particle[_numberOfParticles];

        for (int i = 0; i < particles.Length; i++)
        {
            float x = UnityEngine.Random.Range(-1f, 1f);
            float y = UnityEngine.Random.Range(-1f, 1f);
            float z = UnityEngine.Random.Range(-1f, 1f);
            Vector3 v = new Vector3(x, y, z);
            v.Normalize();
            v *= UnityEngine.Random.Range(0f, 0.5f);
            particles[i].Pos = v;
            particles[i].Pos.z = v.z + 3;
            particles[i].Velocity = new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
            particles[i].Velocity.Normalize();
            particles[i].LifeTime = UnityEngine.Random.Range(2f, 5f);
        }

        _particlesBuffer = new ComputeBuffer(_numberOfParticles, SIZEPARTICLES);
        _particlesBuffer.SetData(particles);
        _kernelID = _computeShader.FindKernel("CSParticle");
        _computeShader.GetKernelThreadGroupSizes(_kernelID, out uint threadX, out _, out _);
        _groupSizeX = Mathf.CeilToInt((float)_numberOfParticles / threadX);
        _computeShader.SetBuffer(_kernelID, "particleBuffer", _particlesBuffer);
        _material.SetBuffer("particleBuffer", _particlesBuffer);
        _material.SetInt("_PointSize", _pointSize);
    }

    private struct Particle
    {
        public Vector3 Pos;
        public Vector3 Velocity;
        public float LifeTime;
    }
}
