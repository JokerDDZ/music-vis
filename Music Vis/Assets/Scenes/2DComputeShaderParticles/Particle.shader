Shader "Custom/Particle"
{
    Properties
    {
        _PointSize("PointSize",Float) = 5.0
    }

    SubShader
    {
        Pass
        {
        Tags { "RenderType"="Opaque" }
        LOD 200
        Blend SrcAlpha one
        
        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma vertex vert
        #pragma fragment frag
        #pragma target 5.0
        
        uniform float _PointSize;
        uniform float Distance;
        uniform float4 BaseColor;

        #include "UnityCG.cginc"

        struct Particle
        {
            float3 position;
            float3 velocity;
            float lifeTime;
        };

        StructuredBuffer<Particle> particleBuffer;

        struct v2f
        {
            float4 position:SV_POSITION;
            float4 color:COLOR;
            float lifeTime:LIFE;
            float size:PSIZE;
        };

        v2f vert(uint vertex_id: SV_VertexID,uint instance_id:SV_INSTANCEID)
        {
            v2f o = (v2f)0;

            o.color = BaseColor * Distance * 2.0;
            o.position = UnityObjectToClipPos(float4(particleBuffer[instance_id].position,1));
            o.size = _PointSize;
            return o;
        }

        float4 frag(v2f i):COLOR
        {
            return i.color;
        }

        ENDCG
        }
    }
    FallBack Off
}
